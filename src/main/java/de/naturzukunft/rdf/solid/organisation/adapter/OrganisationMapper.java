package de.naturzukunft.rdf.solid.organisation.adapter;

import java.net.URL;
import java.util.Optional;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.VCARD4;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;


@Service
public class OrganisationMapper {

	public Organisation toOrganisation(URL profileUrl, org.apache.jena.rdf.model.Model model) {

		Organisation organisation = new Organisation();

		getItResource(model).ifPresent(it -> {
		
//		it.listProperties().forEachRemaining(System.out::println);
			organisation.setWebId(profileUrl.toString());
			getProperty(it, VCARD4.n).ifPresent(organisation::setName);
			getProperty(it, VCARD4.note).ifPresent(organisation::setNote);
			getProperty(it, VCARD4.hasGeo).ifPresent(organisation::setGeoUri);
			getProperty(it, VCARD4.country_name).ifPresent(organisation::setCountryNname);
			getProperty(it, VCARD4.locality).ifPresent(organisation::setLocality);
			getProperty(it, VCARD4.postal_code).ifPresent(organisation::setPostalCode);
			getProperty(it, VCARD4.region).ifPresent(organisation::setRegion);
			getProperty(it, VCARD4.street_address).ifPresent(organisation::setStreetAddress);			
//	private String mobileEmail;
//	private String personalEmail;
//	private String unlabeledEmail;
//	private String workEmail;
//	private String unlabeledPhone;
//	private String homePhone;
//	private String mobilePhone;
//	private String workPhone;
//	private String fax;
			getProperty(it, FOAF.homepage).ifPresent(organisation::setHomepage);
			getProperty(it, VCARD4.category).ifPresent(organisation::setCategories);
//			getProperty(it, FOAF.logo).ifPresent(organisation::setLogoUrl);
//	private String logoUrl;
//	private String logoLinkUrl;


		});
		return organisation;
	}

	private Optional<String> getProperty(Resource it, Property property) {
		Statement propertyResourceValue = it.getProperty(property);
		Optional<String> result = Optional.ofNullable(propertyResourceValue).map(val -> val.getObject().toString()); 
		return result; 
	}

	private Optional<Resource> getItResource(org.apache.jena.rdf.model.Model model) {
	
		// get the statement with the 'primary topic' property  
		Optional<Statement> primaryTopicStatementOptional = model.listStatements(null, FOAF.primaryTopic, (RDFNode)null).toList().stream().findFirst();
		
		// get the subject (as string) out of the statement with the 'primary topic' property
		Optional<String> primaryTopicResourceUrlAsStringOptional = primaryTopicStatementOptional.map(stmt -> stmt.getObject().toString());
		
		// get the primary topic resource as String
		Optional<String> itResourceUrlAsStringOptional = primaryTopicResourceUrlAsStringOptional.map(primaryTopicResourceUrlAsString -> model.getResource(primaryTopicResourceUrlAsString).toString());
		
		// get the primary topic resource out of the model
		return itResourceUrlAsStringOptional.map(itResource -> model.getResource(itResource));
	}
	
	public org.apache.jena.rdf.model.Model toModel(URL webId, URL namespace, Organisation organisation) {
		
		String namespaceStr = namespace.toString();
		
		if(!namespaceStr.endsWith("/")) {
			namespaceStr += "/";
		}
		
		String namespaceItStr = namespaceStr.substring(0,  namespaceStr.length()-1).concat("#it");
//		String namespaceItStr = StringUtils.replace(inString, oldPattern, newPattern)(namespaceStr, "/", "").concat("#it"); 
		
		org.apache.jena.rdf.model.Model model = ModelFactory.createDefaultModel();
		model.setNsPrefix("foaf", "http://xmlns.com/foaf/0.1/");
//		model.setNsPrefix("org", namespace);
//		model.setNsPrefix("", namespace+"card#");
		model.setNsPrefix("schema", "https://schema.org/");
		model.setNsPrefix("vcard", "http://www.w3.org/2006/vcard/ns#");
			
//		String namespaceOfCardString = namespace + "card";
//		Resource namespaceOfCard = model.createResource(namespaceOfCardString);
//		String namespaceOfItString = namespace + "card#it";
//		Resource namespaceOfIt = model.createResource(namespaceOfItString);
		
		Resource namespaceOfProfile = model.createResource(namespaceStr);
		Resource namespaceOfIt = model.createResource(namespaceItStr);
		model.setNsPrefix("", namespaceStr);
		model.setNsPrefix("it", namespaceItStr);
		
		model.add(namespaceOfProfile, FOAF.primaryTopic, namespaceOfIt);
		model.add(namespaceOfProfile, FOAF.maker, webId.toString());		
		
		model.add(namespaceOfIt, RDF.type, VCARD4.Organization);
		model.add(namespaceOfIt, RDF.type, org.apache.jena.sparql.vocabulary.FOAF.Organization);
		model.add(namespaceOfIt, RDF.type, model.createResource("https://schema.org/Organization"));
		
		addProperty(model, namespaceOfIt, VCARD4.n, organisation.getName());
		
		addProperty(model, namespaceOfIt, VCARD4.note, organisation.getNote());
		addProperty(model, namespaceOfIt, VCARD4.hasGeo, organisation.getGeoUri());

		if(organisation.hasAddress()) {
			Resource address = model.createResource(namespaceStr.toString() + "address");
			model.add(namespaceOfIt, VCARD4.hasAddress, address);
			addProperty(model, address, VCARD4.street_address, organisation.getStreetAddress());
			addProperty(model, address, VCARD4.postal_code, organisation.getPostalCode());
			addProperty(model, address, VCARD4.country_name, organisation.getCountryNname());
			addProperty(model, address, VCARD4.locality, organisation.getLocality());
			addProperty(model, address, VCARD4.region, organisation.getRegion());
		}		

//		Resource mobileEmail = model.createResource(namespaceOfIt.toString() + "/mobileEmail");
//		model.add(mobileEmail, VCARD4.email, model.createLiteral(organisation.getMobileEmail()));
//		model.add(namespaceOfIt, VCARD4.hasEmail, mobileEmail);
//
//		Resource personalEmail = model.createResource(namespaceOfIt.toString() + "/personalEmail");
//		model.add(personalEmail, VCARD4.email, model.createLiteral(organisation.getPersonalEmail()));
//		model.add(namespaceOfIt, VCARD4.hasEmail, personalEmail);
//		
//		Resource unlabeledEmail = model.createResource(namespaceOfIt.toString() + "/unlabeledEmail");
//		model.add(unlabeledEmail, VCARD4.email, model.createLiteral(organisation.getUnlabeledEmail()));
//		model.add(namespaceOfIt, VCARD4.hasEmail, unlabeledEmail);
//		
//		Resource workEmail = model.createResource(namespaceOfIt.toString() + "/workEmail");
//		model.add(workEmail, VCARD4.email, model.createLiteral(organisation.getWorkEmail()));
//		model.add(namespaceOfIt, VCARD4.hasEmail, workEmail);
//
//		Resource unlabeledPhone = model.createResource(namespaceOfIt.toString() + "/unlabeledPhone");
//		model.add(unlabeledPhone, VCARD4.tel, model.createLiteral(organisation.getUnlabeledPhone()));
//		model.add(namespaceOfIt, VCARD4.hasTelephone, unlabeledPhone);
//
//		Resource homePhone = model.createResource(namespaceOfIt.toString() + "/homePhone");
//		model.add(homePhone, VCARD4.tel, model.createLiteral(organisation.getHomePhone()));
//		model.add(namespaceOfIt, VCARD4.hasTelephone, homePhone);
//		
//		Resource mobilePhone = model.createResource(namespaceOfIt.toString() + "/mobilePhone");
//		model.add(mobilePhone, VCARD4.tel, model.createLiteral(organisation.getMobilePhone()));
//		model.add(namespaceOfIt, VCARD4.hasTelephone, mobilePhone);
//
//		Resource workPhone = model.createResource(namespaceOfIt.toString() + "/workPhone");
//		model.add(workPhone, VCARD4.tel, model.createLiteral(organisation.getWorkPhone()));
//		model.add(namespaceOfIt, VCARD4.hasTelephone, workPhone);
//
//		Resource fax = model.createResource(namespaceOfIt.toString() + "/fax");
//		model.add(fax, VCARD4.tel, model.createLiteral(organisation.getFax()));
//		model.add(namespaceOfIt, VCARD4.hasTelephone, fax);
		
		addProperty(model, namespaceOfIt, FOAF.homepage, organisation.getHomepage());

		if(organisation.hasCategories()) {
			for ( String category : organisation.getCategories()) {
				model.add(namespaceOfIt, VCARD4.category, model.createLiteral(category));
			}
		}		
		
		if(organisation.hasLogo()) {
			Resource logo = model.createResource(namespaceStr.toString() + "logo");		
			model.add(namespaceOfIt, FOAF.logo, logo);
			addProperty(model, logo, VCARD4.url, organisation.getLogoLinkUrl());
			addProperty(model, logo, VCARD4.photo, organisation.getLogoUrl());
		}		
		
		return model;
	}

	private void addProperty(Model model, Resource subject, Property property, String object) {
		if(object != null && StringUtils.hasText(object)) {
			model.add(subject, property, model.createLiteral(object));
		}
	}
}
