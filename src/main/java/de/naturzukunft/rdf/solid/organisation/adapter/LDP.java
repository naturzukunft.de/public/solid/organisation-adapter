package de.naturzukunft.rdf.solid.organisation.adapter;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

public class LDP {
	/**
	 * The namespace of the vocabulary as a resource
	 */
	public static final Resource NAMESPACE = ResourceFactory.createResource(LDPConstants.getNSURI());
	
	public static final Property contains = property(LDPConstants.PROP_CONTAINS);
	public static final Property member = property(LDPConstants.PROP_MEMBER);
	public static final Property membershipResource = property(LDPConstants.PROP_MEMBERSHIP_RESOURCE);
	public static final Property hasMemberRelation = property(LDPConstants.PROP_HAS_MEMBER_RELATION);
	public static final Property isMemberOfRelation = property(LDPConstants.PROP_IS_MEMBER_OF_RELATION);
	public static final Property pageOf = property(LDPConstants.PROP_PAGEOF);
	public static final Property nextPage = property(LDPConstants.PROP_NEXTPAGE);
	public static final Property containerSortPredicates = property(LDPConstants.PROP_CONTAINER_SORT_PREDICATE);
 
	public static final Resource RDFSource = resource(LDPConstants.CLASS_RDFSOURCE);
	public static final Resource Container = resource(LDPConstants.CLASS_CONTAINER);
	public static final Resource BasicContainer = resource(LDPConstants.CLASS_BASIC_CONTAINER);
	public static final Resource DirectContainer = resource(LDPConstants.CLASS_DIRECT_CONTAINER);
	public static final Resource IndirectContainer = resource(LDPConstants.CLASS_INDIRECT_CONTAINER);
	public static final Resource Page = resource(LDPConstants.CLASS_PAGE);

	protected static final Resource resource(String name)
	{
		return ResourceFactory.createResource(name); 
	}

	protected static final Property property(String name)
	{ 
		return ResourceFactory.createProperty(name);
	}

}
