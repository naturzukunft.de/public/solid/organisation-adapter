package de.naturzukunft.rdf.solid.organisation.adapter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Service;

@Service
public class AuthInfo {

	private OAuth2AuthorizedClientService auth2AuthorizedClientService;
	
	public AuthInfo(OAuth2AuthorizedClientService auth2AuthorizedClientService) {
		this.auth2AuthorizedClientService = auth2AuthorizedClientService;
	}
	
	public Optional<AuthInfoData> getAuthInfoData() {
		
		Optional<AuthInfoData> result = Optional.empty();
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Optional<OAuth2AuthorizedClient> client = Optional.ofNullable(getOAuth2AuthorizedClient(auth));

		if(client.isPresent()) {
			AuthInfoData autInfo = new AuthInfoData();
			autInfo.setWebId(extractWebId(auth));
			autInfo.setAccessToken(client.get().getAccessToken().getTokenValue());
			autInfo.setClientId(client.get().getClientRegistration().getClientId());
			result = Optional.of(autInfo);
		}
		return result;
	}

	private OAuth2AuthorizedClient getOAuth2AuthorizedClient(Authentication auth) {
		OAuth2AuthenticationToken oauthToken = (OAuth2AuthenticationToken) auth;

		OAuth2AuthorizedClient client = auth2AuthorizedClientService
				.loadAuthorizedClient(oauthToken.getAuthorizedClientRegistrationId(), oauthToken.getName());
		return client;
	}

	private URL extractWebId(Authentication auth) {
		try {
			return new URL(auth.getName());
		} catch (MalformedURLException e) {
			throw new RuntimeException("webId -> MalformedURLException");
		}
	}
}
