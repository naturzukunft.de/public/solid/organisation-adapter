package de.naturzukunft.rdf.solid.organisation.adapter;

import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Selector;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.reactive.function.client.WebClient;

import de.naturzukunft.rdf.solid.pod.adapter.BuilderFactory;
import de.naturzukunft.rdf.solid.pod.adapter.DPopProvider;
import de.naturzukunft.rdf.solid.pod.adapter.PodResponse;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Slf4j
public class PodStorage implements Storage {
	
	private BuilderFactory builderFactory;
	private DPopProvider dPopProvider;
	private WebClient webClient;
	private AuthInfoData authInfoData;

	public PodStorage(AuthInfoData authInfoData, BuilderFactory builderFactory, DPopProvider dPopProvider, WebClient webClient) {
		this.authInfoData = authInfoData;
		this.builderFactory = builderFactory;
		this.dPopProvider = dPopProvider;
		this.webClient = webClient;
	}
	
	@Override
	public String delete(URL namespace) {
		Mono<ResponseEntity<String>> podMono = webClient
				.delete().uri(namespace.toString())
				.header("Content-Type", "text/turtle")
				.header("Authorization", "DPoP " + authInfoData.getAccessToken())
				.header("DPoP", dPopProvider.getDPopToken(authInfoData.getClientId(), HttpMethod.DELETE, URI.create(namespace.toString())))
				.retrieve()
				.toEntity(String.class);
		ResponseEntity<String> response = podMono.block();
		return response.getBody();
	}

	@Override
	public String read(URL namespace) {
		Mono<ResponseEntity<String>> podMono = webClient
				.get().uri(namespace.toString())
				.header("Content-Type", "text/turtle")
				.header("Authorization", "DPoP " + authInfoData.getAccessToken())
				.header("DPoP", dPopProvider.getDPopToken(authInfoData.getClientId(), HttpMethod.GET, URI.create(namespace.toString())))
				.retrieve()
				.toEntity(String.class);
		ResponseEntity<String> response = podMono.block();
		return response.getBody();
	}
	
	@Override
	public String store(String content, URL namespace, String slug) {
		SecurityContextHolder.getContext().getAuthentication().getCredentials();
		PodResponse response = builderFactory.createResourceBuilder(authInfoData.getClientId(), authInfoData.getAccessToken())
				.withContent(content)
				.withLink("<http://www.w3.org/ns/ldp#Resource>; rel=\"type\"")
				.withSlug(slug)
				.withUrl(namespace.toString())
				.build();
		
		String location = "";
		List<String> locations = response.getHeaders().get("Location");
		if(!(locations == null || locations.isEmpty())) {
			location = locations.get(0);
		}
		return location;
	}

	@Override
	public List<URL> getContainerItems(URL namespace) {
		List<URL> response = new ArrayList<>();
		String modelAsString = read(namespace);
			
		Model model = ModelFactory.createDefaultModel();
		model.read(new StringReader(modelAsString), namespace.toString(), "TTL");
		
		model.listStatements(null, LDP.contains, (RDFNode)null).forEachRemaining(stmt -> {
			String subject = stmt.getObject().toString();
			try {
				response.add(new URL(subject));
			} catch (MalformedURLException e) {
				log.error("error creating url of " + subject, e);
			}
		});
		return response;
	}

	@Override
	public List<URL> getContainerItemsFilteredBySelector(URL namespace, Selector selector) {
		List<URL> response = new ArrayList<>();
		String modelAsString = read(namespace);
			
		Model model = ModelFactory.createDefaultModel();
		model.read(new StringReader(modelAsString), namespace.toString(), "TTL");

		model.listStatements(selector).forEachRemaining(stmt -> {
			try {
				response.add(new URL(stmt.getObject().toString()));
			} catch (MalformedURLException e) {
				log.error("error creating url of " + stmt.getString(), e);
			}
		});
		return response;
	}
}
